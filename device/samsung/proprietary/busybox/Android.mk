ifeq ($(BOARD_USE_BUSYBOX), true)

BUSYBOX_EXT_COMPILE := arm-none-linux-gnueabi-
ANDROID_ROOT := $(shell pwd)
LOCAL_PATH := $(call my-dir)
BUSYBOX_PATH := $(LOCAL_PATH)

include $(CLEAR_VARS)

TOOL_CFLAGS := --static
TOOL_LDFLAGS := --static

BUILD_BUSYBOX_DRIVER := $(LOCAL_PATH)/busybox
$(BUILD_BUSYBOX_DRIVER): PRIVATE_CUSTOM_TOOL = \
	cp -a $(BUSYBOX_PATH)/.config-optimize $(BUSYBOX_PATH)/.config; \
	make -C $(BUSYBOX_PATH) ARCH=arm CROSS_COMPILE=$(BUSYBOX_EXT_COMPILE) CFLAGS=$(TOOL_CFLAGS) LDFLAGS=$(TOOL_LDFLAGS) CONFIG_PREFIX=$(ANDROID_ROOT)/$(TARGET_ROOT_OUT) install

$(BUILD_BUSYBOX_DRIVER):
	$(transform-generated-source)
LOCAL_GENERATED_SOURCES += $(BUILD_BUSYBOX_DRIVER)

ALL_PREBUILT += $(TARGET_ROOT_OUT)/bin/busybox
$(TARGET_ROOT_OUT)/bin/busybox : $(BUILD_BUSYBOX_DRIVER) | $(ACP)
	$(transform-prebuilt-to-target)

endif
