ifeq ($(filter-out s5pc110,$(TARGET_BOARD_PLATFORM)),)                                                       

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

ifeq ($(BOARD_USES_FIMGAPI),true)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include

LOCAL_SRC_FILES:= \
	SecFimg.cpp   \
	SecFimgC110.cpp

LOCAL_SHARED_LIBRARIES:= liblog libutils libbinder

LOCAL_MODULE:= libfimg

LOCAL_MODULE_TAGS = optional
LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)

endif

endif

