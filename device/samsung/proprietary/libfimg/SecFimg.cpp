/*
**
** Copyright 2009 Samsung Electronics Co, Ltd.
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
**
** @author Sangwoo, Park(sw5771.park@samsung.com)
** @date   2010-07-01
**
*/

///////////////////////////////////////////////////
// include
///////////////////////////////////////////////////
#define LOG_NDEBUG 0
#define LOG_TAG "SecFimg"
#include <utils/Log.h>

#include "SecFimg.h"

//---------------------------------------------------------------------------//
// Global Function
//---------------------------------------------------------------------------//
#ifndef REAL_DEBUG
    void VOID_FUNC(const char* format, ...)
    {}
#endif

#define GET_32BPP_FRAME_SIZE(w, h)  (((w) * (h)) << 2)
#define GET_24BPP_FRAME_SIZE(w, h)  (((w) * (h)) * 3)
#define GET_16BPP_FRAME_SIZE(w, h)  (((w) * (h)) << 1)

//---------------------------------------------------------------------------//
// SecFimg
//---------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
// Method Function Implementation
//---------------------------------------------------------------------------//

SecFimg::SecFimg()
{
    m_flagCreate = false;
}

SecFimg::~SecFimg()
{
    if(m_flagCreate == true)
        PRINT("%s::this is not Destroyed fail \n", __func__);
}

bool SecFimg::Create(void)
{
    bool ret = false;

    if(t_Lock() == false)
    {
        PRINT("%s::t_Lock() fail \n", __func__);
        goto CREATE_DONE;
    }

    if(m_flagCreate == true)
    {
        PRINT("%s::Already Created fail \n", __func__);
        goto CREATE_DONE;
    }

    if(t_Create() == false)
    {
        PRINT("%s::t_Create() fail \n", __func__);
        goto CREATE_DONE;
    }

    m_flagCreate = true;

    ret = true;

CREATE_DONE :

    t_UnLock();

    return ret;
}

bool SecFimg::Destroy(void)
{
    bool ret = false;

    if(t_Lock() == false)
    {
        PRINT("%s::t_Lock() fail \n", __func__);
        goto DESTROY_DONE;
    }

    if(m_flagCreate == false)
    {
        PRINT("%s::Already Destroyed fail \n", __func__);
        goto DESTROY_DONE;
    }
    if(t_Destroy() == false)
    {
        PRINT("%s::t_Destroy() fail \n", __func__);
        goto DESTROY_DONE;
    }

    m_flagCreate = false;

    ret = true;

DESTROY_DONE :

    t_UnLock();

    return ret;
}

bool SecFimg::Stretch(FimgRect * src, FimgRect * dst, FimgFlag * flag)
{
    bool ret = false;

    if(t_Lock() == false)
    {
        PRINT("%s::t_Lock() fail \n", __func__);
        goto STRETCH_DONE;
    }

    if(m_flagCreate == false)
    {
        PRINT("%s::This is not Created fail \n", __func__);
        goto STRETCH_DONE;
    }

    // sw5771.park : do we need more argument check? (src->virt_addr ?)
    /*
    if(src == NULL || dst == NULL || flag == NULL)
    {
        PRINT("%s::Argument is NULL fail \n", __func__);
        goto STRETCH_DONE;
    }
    if(flag->rotate_val <= ROTATE_BASE || ROTATE_MAX <= flag->rotate_val)
    {
        PRINT("%s::flag->rotate_val(%d) value is out of bound fail \n", __func__, flag->rotate_val);
        goto STRETCH_DONE;
    }
    if(flag->alpha < ALPHA_MIN || ALPHA_OPAQUE < flag->alpha)
    {
        PRINT("%s::flag->alpha(%d) value is out of bound fail \n", __func__, flag->alpha);
        goto STRETCH_DONE;
    }
    */
    if(t_Stretch(src, dst, flag) == false)
    {
        PRINT("%s::t_Stretch() fail\n", __func__);
        goto STRETCH_DONE;
    }

    ret = true;

STRETCH_DONE :

    t_UnLock();

    return ret;
}

bool SecFimg::t_Create(void)
{
    PRINT("%s::This is empty virtual function fail\n", __func__);
    return false;
}

bool SecFimg::t_Destroy(void)
{
    PRINT("%s::This is empty virtual function fail\n", __func__);
    return false;
}

bool SecFimg::t_Stretch(FimgRect * src, FimgRect * dst, FimgFlag * flag)
{
    PRINT("%s::This is empty virtual function fail\n", __func__);
    return false;
}

bool SecFimg::t_Lock(void)
{
    PRINT("%s::This is empty virtual function fail\n", __func__);
    return false;
}

bool SecFimg::t_UnLock(void)
{
    PRINT("%s::This is empty virtual function fail\n", __func__);
    return false;
}

unsigned int SecFimg::t_FrameSize(FimgRect * rect)
{
    unsigned int frame_size = 0;
    unsigned int size       = 0;

    switch(rect->color_format)
    {
        // 16bpp
        case COLOR_FORMAT_RGB_565   :
            frame_size = GET_16BPP_FRAME_SIZE(rect->full_w, rect->full_h);
            //size = rect->full_w * rect->full_h;
            //frame_size = (size << 1);
            break;

        // 32bpp
        case COLOR_FORMAT_RGBA_8888 :
        case COLOR_FORMAT_ARGB_8888 :
        case COLOR_FORMAT_BGRA_8888 :
        case COLOR_FORMAT_ABGR_8888 :

        case COLOR_FORMAT_RGBX_8888 :
        case COLOR_FORMAT_XRGB_8888 :
        case COLOR_FORMAT_BGRX_8888 :
        case COLOR_FORMAT_XBGR_8888 :
            frame_size = GET_32BPP_FRAME_SIZE(rect->full_w, rect->full_h);
            //size = rect->full_w * rect->full_h;
            //frame_size = (size << 2);
            break;

        // 16bpp
        case COLOR_FORMAT_RGBA_5551 :
        case COLOR_FORMAT_ARGB_1555 :
        case COLOR_FORMAT_BGRA_5551 :
        case COLOR_FORMAT_ABGR_1555 :

        case COLOR_FORMAT_RGBX_5551 :
        case COLOR_FORMAT_XRGB_1555 :
        case COLOR_FORMAT_BGRX_5551 :
        case COLOR_FORMAT_XBGR_1555 :

        case COLOR_FORMAT_RGBA_4444 :
        case COLOR_FORMAT_ARGB_4444 :
        case COLOR_FORMAT_BGRA_4444 :
        case COLOR_FORMAT_ABGR_4444 :

        case COLOR_FORMAT_RGBX_4444 :
        case COLOR_FORMAT_XRGB_4444 :
        case COLOR_FORMAT_BGRX_4444 :
        case COLOR_FORMAT_XBGR_4444 :
            frame_size = GET_16BPP_FRAME_SIZE(rect->full_w, rect->full_h);
            break;

        // 24bpp
        case COLOR_FORMAT_PACKED_RGB_888 :
        case COLOR_FORMAT_PACKED_BGR_888 :
            frame_size = GET_24BPP_FRAME_SIZE(rect->full_w, rect->full_h);
            break;

        // 18bpp
        case COLOR_FORMAT_YUV_420SP :
        case COLOR_FORMAT_YUV_420P  :
        case COLOR_FORMAT_YUV_420I  :
            size = rect->full_w * rect->full_h;
            //frame_size = width * height * 3 / 2;
            // sw5771.park : very curious...
            //frame_size = size + (( size / 4) * 2);
            frame_size = size + (( size >> 2) << 1);
            break;

        // 16bpp
        case COLOR_FORMAT_YUV_422SP :
        case COLOR_FORMAT_YUV_422P  :
        case COLOR_FORMAT_YUV_422I  :
        case COLOR_FORMAT_YUYV      :
            frame_size = GET_16BPP_FRAME_SIZE(rect->full_w, rect->full_h);
            break;

        default :
            PRINT("%s::no matching source colorformat(%d), w(%d), h(%d) fail\n",
                    __func__, rect->color_format, rect->full_w, rect->full_h);
            break;
    }
    return frame_size;
}


bool SecFimg::t_CopyFrame(FimgRect * src, FimgRect * dst)
{
    // sw5771.park : This Function assume
    // src->color_format == dst->color_format
    // src->w           == dst->w
    // src->h           == dst->h

    unsigned int bppShift = 0;

    unsigned int realSrcFullW  = 0;
    unsigned int realSrcX      = 0;
    unsigned int realDstFullW  = 0;
    unsigned int realDstX      = 0;

    unsigned int realW         = 0;

    unsigned char * curSrcAddr = src->virt_addr;
    unsigned char * curDstAddr = dst->virt_addr;

    switch(src->color_format)
    {
        // 16bit
        case COLOR_FORMAT_RGB_565   :
            bppShift = 1;
            break;

        // 32bit
        case COLOR_FORMAT_RGBA_8888 :
        case COLOR_FORMAT_ARGB_8888 :
        case COLOR_FORMAT_BGRA_8888 :
        case COLOR_FORMAT_ABGR_8888 :

        case COLOR_FORMAT_RGBX_8888 :
        case COLOR_FORMAT_XRGB_8888 :
        case COLOR_FORMAT_BGRX_8888 :
        case COLOR_FORMAT_XBGR_8888 :
            bppShift = 2;
            break;

        // 16bit
        case COLOR_FORMAT_RGBA_5551 :
        case COLOR_FORMAT_ARGB_1555 :
        case COLOR_FORMAT_BGRA_5551 :
        case COLOR_FORMAT_ABGR_1555 :

        case COLOR_FORMAT_RGBX_5551 :
        case COLOR_FORMAT_XRGB_1555 :
        case COLOR_FORMAT_BGRX_5551 :
        case COLOR_FORMAT_XBGR_1555 :

        case COLOR_FORMAT_RGBA_4444 :
        case COLOR_FORMAT_ARGB_4444 :
        case COLOR_FORMAT_BGRA_4444 :
        case COLOR_FORMAT_ABGR_4444 :

        case COLOR_FORMAT_RGBX_4444 :
        case COLOR_FORMAT_XRGB_4444 :
        case COLOR_FORMAT_BGRX_4444 :
        case COLOR_FORMAT_XBGR_4444 :
            bppShift = 1;
            break;

        //case COLOR_FORMAT_RGB_888   :
        //case COLOR_FORMAT_BGR_888   :
        default:
            PRINT("%s::no matching source colorformat(%d), w(%d), h(%d) fail\n",
                  __func__, src->color_format, src->full_w, src->full_h);
            return false;
            break;
    }

    if(    src->full_w == dst->full_w
        && src->full_w == src->w)
    {
        // if the rectangles of src & dst are exactly the same.
        if(   src->full_h == dst->full_h
           && src->full_h == src->h)
        {
            unsigned int fullFrameSize  = 0;

            fullFrameSize = ((src->full_w * src->full_h) << bppShift);
            memcpy(dst->virt_addr, src->virt_addr, fullFrameSize);
        }
        // src y and dst y is different..
        else
        {
            realSrcFullW = (src->full_w  << bppShift);
            realDstFullW = (dst->full_w  << bppShift);

            realW        = (src->w       << bppShift);

            curSrcAddr += (realSrcFullW * src->y);
            curDstAddr += (realDstFullW * dst->y);

            memcpy(curDstAddr, curSrcAddr, realW * src->h);
        }
    }
    else
    {
        realSrcFullW = (src->full_w  << bppShift);
        realSrcX     = (src->x       << bppShift);

        realDstFullW = (dst->full_w  << bppShift);
        realDstX     = (dst->x       << bppShift);

        realW        = (src->w      << bppShift);

        curSrcAddr += (realSrcFullW * src->y);
        curDstAddr += (realDstFullW * dst->y);

        for(unsigned int i = 0; i < src->h; i++)
        {
            memcpy(curDstAddr + realDstX,
                   curSrcAddr + realSrcX,
                   realW);

            curSrcAddr += realSrcFullW;
            curDstAddr += realDstFullW;
        }
    }

    return true;
}


//---------------------------------------------------------------------------//
// extern function
//---------------------------------------------------------------------------//
extern "C" int stretchSecFimg(FimgRect * src, FimgRect * dst, FimgFlag * flag)
{
    SecFimg * fimg = createSecFimg();
    if(fimg == NULL)
    {
        PRINT("%s::createSecFimg() fail \n", __func__);
        return -1;
    }

    if(fimg->Stretch(src, dst, flag) == false)
    {
        PRINT("%s::Stretch() fail \n", __func__);

        if(fimg != NULL)
            destroySecFimg(fimg);

        return -1;
    }

    if(fimg != NULL)
        destroySecFimg(fimg);

    return 0;
}
