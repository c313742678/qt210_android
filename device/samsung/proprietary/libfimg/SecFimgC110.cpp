/*
**
** Copyright 2009 Samsung Electronics Co, Ltd.
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
**
** @author Sangwoo, Park(sw5771.park@samsung.com)
** @date   2010-07-01
**
*/

///////////////////////////////////////////////////
// include
///////////////////////////////////////////////////
#define LOG_NDEBUG 0
#define LOG_TAG "SecFimgC110"
#include <utils/Log.h>

#include "SecFimgC110.h"

namespace android
{

//---------------------------------------------------------------------------//
// SecFimgC110
//---------------------------------------------------------------------------//

Mutex      SecFimgC110::m_instanceLock;
int        SecFimgC110::m_curSecFimgC110Index = 0;
int        SecFimgC110::m_numOfInstance    = 0;
SecFimg *  SecFimgC110::m_ptrSecFimgList[NUMBER_FIMG_LIST] = {NULL, };

//---------------------------------------------------------------------------//

SecFimgC110::SecFimgC110()
         : m_g2dFd(0),
           m_g2dVirtAddr(NULL),
           m_g2dPhysAddr(0),
           m_g2dSize(0),
           m_g2dSrcVirtAddr(NULL),
           m_g2dSrcPhysAddr(0),
           m_g2dSrcSize(0),
           m_g2dDstVirtAddr(NULL),
           m_g2dDstPhysAddr(0),
           m_g2dDstSize(0)
{
    m_lock = new Mutex(Mutex::SHARED, "SecFimgC110");
}

SecFimgC110::~SecFimgC110()
{
    delete m_lock;
}

SecFimg * SecFimgC110::CreateInstance()
{
    Mutex::Autolock autolock(m_instanceLock);

    SecFimg * ptrFimg = NULL;

    // Using List like RingBuffer...
    for(int i = m_curSecFimgC110Index; i < NUMBER_FIMG_LIST; i++)
    {
        if(m_ptrSecFimgList[i] == NULL)
            m_ptrSecFimgList[i] = new SecFimgC110;

        if(m_ptrSecFimgList[i]->FlagCreate() == false)
        {
            if(m_ptrSecFimgList[i]->Create() == false)
            {
                PRINT("%s::Create(%d) fail\n", __func__, i);
                goto CreateInstance_End;
            }
            else
                m_numOfInstance++;
        }

        if(i < NUMBER_FIMG_LIST - 1)
            m_curSecFimgC110Index = i + 1;
        else
            m_curSecFimgC110Index = 0;

        ptrFimg = m_ptrSecFimgList[i];
        goto CreateInstance_End;
    }

CreateInstance_End :

    return ptrFimg;
}

void SecFimgC110::DestroyInstance(SecFimg * ptrSecFimg)
{
    Mutex::Autolock autolock(m_instanceLock);

    for(int i = 0; i < NUMBER_FIMG_LIST; i++)
    {
        if(   m_ptrSecFimgList[i] != NULL
           && m_ptrSecFimgList[i] == ptrSecFimg)
        {
            if(   m_ptrSecFimgList[i]->FlagCreate() == true
               && m_ptrSecFimgList[i]->Destroy()    == false)
            {
                PRINT("%s::Destroy() fail\n", __func__);
            }
            else // Destroy Succeed..
            {
                SecFimgC110 * tempSecFimgC110 =  (SecFimgC110 *)m_ptrSecFimgList[i];
                delete tempSecFimgC110;
                m_ptrSecFimgList[i] = NULL;

                m_numOfInstance--;
            }

            break;
        }
    }
}

void SecFimgC110::DestroyAllInstance(void)
{
    Mutex::Autolock autolock(m_instanceLock);

    for(int i = 0; i < NUMBER_FIMG_LIST; i++)
    {
        if(m_ptrSecFimgList[i] != NULL)
        {
            if(   m_ptrSecFimgList[i]->FlagCreate() == true
               && m_ptrSecFimgList[i]->Destroy()    == false)
            {
                    PRINT("%s::Destroy() fail\n", __func__);
            }
            else // Destroy Succeed....
            {
                SecFimgC110 * tempSecFimgC110 =  (SecFimgC110 *)m_ptrSecFimgList[i];
                delete tempSecFimgC110;
                m_ptrSecFimgList[i] = NULL;
            }
        }
    }
}


bool SecFimgC110::t_Create(void)
{
    bool ret = true;

    if(m_CreateG2D() == false)
    {
        PRINT("%s::m_CreateG2D() fail \n", __func__);

        if(m_DestroyG2D() == false)
            PRINT("%s::m_DestroyG2D() fail \n", __func__);

        ret = false;
    }

    return ret;
}

bool SecFimgC110::t_Destroy(void)
{
    bool ret = true;

    if(m_DestroyG2D() == false)
    {
        PRINT("%s::m_DestroyG2D() fail \n", __func__);
        ret = false;
    }

    return ret;
}

bool SecFimgC110::t_Stretch(FimgRect * src, FimgRect * dst, FimgFlag * flag)
{
    #ifdef CHECK_FIMGC110_PERFORMANCE
        #define   NUM_OF_STEP (10)
        StopWatch    stopWatch("CHECK_FIMGC110_PERFORMANCE");
        const char * stopWatchName[NUM_OF_STEP];
        nsecs_t      stopWatchTime[NUM_OF_STEP];
        int          stopWatchIndex = 0;
    #endif // CHECK_FIMGC110_PERFORMANCE

    FimgRect * srcMidRect = src;
    FimgRect * dstMidRect = dst;
    unsigned int srcMidRectSize = 0;
    unsigned int dstMidRectSize = 0;

    // optimized size
    if(src && src->phys_addr == 0)
    {
        FimgRect srcTempMidRect = {0, 0, src->w, src->h, src->w, src->h, src->color_format, m_g2dSrcPhysAddr, m_g2dSrcVirtAddr};

        // use optimized size
        srcMidRect     = &srcTempMidRect;
        srcMidRectSize = t_FrameSize(srcMidRect);

        if(m_g2dSrcSize < srcMidRectSize)
        {
            PRINT("%s::src size is too big fail\n", __func__);
            return false;
        }
    }

    FimgRect dstTempMidRect = {0, 0, dst->w, dst->h, dst->w, dst->h, dst->color_format, m_g2dDstPhysAddr, m_g2dDstVirtAddr};
    if(dst->phys_addr == 0)
    {
        // use optimized size
        dstMidRect = &dstTempMidRect;
        dstMidRectSize = t_FrameSize(dstMidRect);

        if(m_g2dDstSize < dstMidRectSize)
        {
            PRINT("%s::src size is too big fail\n", __func__);
            return false;
        }
    }

    #ifdef CHECK_FIMGC110_PERFORMANCE
        stopWatchName[stopWatchIndex] = "StopWatch Ready";
        stopWatchTime[stopWatchIndex] = stopWatch.elapsedTime();
        stopWatchIndex++;
    #endif // CHECK_FIMGC110_PERFORMANCE

    #ifdef G2D_NONE_BLOCKING_MODE
    {
        if(m_PollG2D(&m_g2dPoll) == false)
        {
            PRINT("%s::m_PollG2D 0() fail\n", __func__);
            return false;
        }
    }
    #endif

    if(src && src->phys_addr == 0)
    {
        if(t_CopyFrame(src, srcMidRect) == false)
        {
            PRINT("%s::t_CopyFrame() fail\n", __func__);
            return false;
        }

        #ifdef CHECK_FIMGC110_PERFORMANCE
            stopWatchName[stopWatchIndex] = "Src Copy";
            stopWatchTime[stopWatchIndex] = stopWatch.elapsedTime();
            stopWatchIndex++;
        #endif // CHECK_FIMGC110_PERFORMANCE

        if(m_CleanG2D(srcMidRect->phys_addr, srcMidRectSize) == false)
        {
            PRINT("%s::m_CleanG2D() fail\n", __func__);
            return false;
        }

        #ifdef CHECK_FIMGC110_PERFORMANCE
            stopWatchName[stopWatchIndex] = "Src CacheClean";
            stopWatchTime[stopWatchIndex] = stopWatch.elapsedTime();
            stopWatchIndex++;
        #endif // CHECK_FIMGC110_PERFORMANCE
    }

    if(dst->phys_addr == 0)
    {
        bool flagDstCopy = false;

        if(flag->alpha_val <= ALPHA_MAX) // <= 255
            flagDstCopy = true;

        if(flagDstCopy == true)
        {
            if(t_CopyFrame(dst, dstMidRect) == false)
            {
                PRINT("%s::t_CopyFrame() fail\n", __func__);
                return false;
            }

            #ifdef CHECK_FIMGC110_PERFORMANCE
                stopWatchName[stopWatchIndex] = "Dst Copy";
                stopWatchTime[stopWatchIndex] = stopWatch.elapsedTime();
                stopWatchIndex++;
            #endif // CHECK_FIMGC110_PERFORMANCE

            if(m_CleanG2D(dstMidRect->phys_addr, dstMidRectSize) == false)
            {
                PRINT("%s::m_CleanG2D() fail\n", __func__);
                return false;
            }

            #ifdef CHECK_FIMGC110_PERFORMANCE
                stopWatchName[stopWatchIndex] = "Dst CacheClean";
                stopWatchTime[stopWatchIndex] = stopWatch.elapsedTime();
                stopWatchIndex++;
            #endif // CHECK_FIMGC110_PERFORMANCE
        }
    }

    if(m_DoG2D(srcMidRect, dstMidRect, flag) == false)
    {
        PRINT("%s::m_DoG2D fail\n", __func__);
        return false;
    }

    #ifdef CHECK_FIMGC110_PERFORMANCE
            stopWatchName[stopWatchIndex] = "G2D(HW) Run";
            stopWatchTime[stopWatchIndex] = stopWatch.elapsedTime();
            stopWatchIndex++;
    #endif // CHECK_FIMGC110_PERFORMANCE

    if(dst->phys_addr == 0)
    {
        if(m_InvalidG2D(dstMidRect->phys_addr, dstMidRectSize) == false)
        {
            PRINT("%s::m_InvalidG2D() fail\n", __func__);
            return false;
        }

        #ifdef CHECK_FIMGC110_PERFORMANCE
                stopWatchName[stopWatchIndex] = "Dst Cache Invalidate";
                stopWatchTime[stopWatchIndex] = stopWatch.elapsedTime();
                stopWatchIndex++;
        #endif // CHECK_FIMGC110_PERFORMANCE

        #ifdef G2D_NONE_BLOCKING_MODE
        {
            if(m_PollG2D(&m_g2dPoll) == false)
            {
                PRINT("%s::m_PollG2D 1() fail\n", __func__);
                return false;
            }
        }
        #endif

        #ifdef CHECK_FIMGC110_PERFORMANCE
                stopWatchName[stopWatchIndex] = "Poll";
                stopWatchTime[stopWatchIndex] = stopWatch.elapsedTime();
                stopWatchIndex++;
        #endif // CHECK_FIMGC110_PERFORMANCE

        if(t_CopyFrame(dstMidRect, dst) == false)
        {
            PRINT("%s::t_CopyFrame() fail\n", __func__);
            return false;
        }

        #ifdef CHECK_FIMGC110_PERFORMANCE
                stopWatchName[stopWatchIndex] = "Dst Copy";
                stopWatchTime[stopWatchIndex] = stopWatch.elapsedTime();
                stopWatchIndex++;
        #endif // CHECK_FIMGC110_PERFORMANCE
    }
    #ifdef G2D_NONE_BLOCKING_MODE
    else
    {
        if(m_PollG2D(&m_g2dPoll) == false)
        {
            PRINT("%s::m_PollG2D 2() fail\n", __func__);
            return false;
        }
    }
    #endif

    #ifdef CHECK_FIMGC110_PERFORMANCE
        m_PrintSecFimgC110Performance(src, dst, stopWatchIndex, stopWatchName, stopWatchTime);
    #endif // CHECK_FIMGC110_PERFORMANCE

    return true;
}

bool SecFimgC110::t_Lock(void)
{
    m_lock->lock();
    return true;
}

bool SecFimgC110::t_UnLock(void)
{
    m_lock->unlock();
    return true;
}

bool SecFimgC110::m_CreateG2D(void)
{
    void * mmap_base;

    if(m_g2dFd != 0)
    {
        PRINT("%s::m_g2dFd(%d) is not 0 fail\n", __func__, m_g2dFd);
        return false;
    }

    #ifdef G2D_NONE_BLOCKING_MODE
        m_g2dFd = open(SEC_G2D_DEV_NAME, O_RDWR | O_NONBLOCK);
    #else
        m_g2dFd = open(SEC_G2D_DEV_NAME, O_RDWR);
    #endif
    if(m_g2dFd < 0)
    {
        PRINT("%s::open(%s) fail(%s)\n", __func__, SEC_G2D_DEV_NAME, strerror(errno));
        m_g2dFd = 0;
        return false;
    }
    #ifdef G2D_NONE_BLOCKING_MODE
        memset(&m_g2dPoll, 0, sizeof(m_g2dPoll));
        m_g2dPoll.fd     = m_g2dFd;
        m_g2dPoll.events = POLLOUT | POLLERR;
    #endif

    unsigned int g2d_phys_addr = 0;
    if(ioctl(m_g2dFd, G2D_GET_MEMORY, &g2d_phys_addr) < 0)
    {
        PRINT("%s::G2D_GET_MEMORY fail\n", __func__);
        return false;
    }

    unsigned int g2d_total_size = 0;
    if(ioctl(m_g2dFd, G2D_GET_MEMORY_SIZE, &g2d_total_size) < 0)
    {
        PRINT("%s::G2D_GET_MEMORY_SIZE fail\n", __func__);
        return false;
    }

    mmap_base = mmap(0, g2d_total_size, PROT_READ|PROT_WRITE, MAP_SHARED, m_g2dFd, 0);
    if(mmap_base == MAP_FAILED)
    {
        PRINT("%s::mmap failed : %d (%s)", __func__, errno, strerror(errno));
        return false;
    }

    m_g2dVirtAddr    = (unsigned char *)mmap_base;
    m_g2dPhysAddr    = g2d_phys_addr;
    m_g2dSize        = g2d_total_size;

    m_g2dSrcVirtAddr = m_g2dVirtAddr;
    m_g2dSrcPhysAddr = m_g2dPhysAddr;
    m_g2dSrcSize     = m_g2dSize >> 1;

    m_g2dDstVirtAddr = m_g2dSrcVirtAddr + m_g2dSrcSize;
    m_g2dDstPhysAddr = m_g2dSrcPhysAddr + m_g2dSrcSize;
    m_g2dDstSize     = m_g2dSrcSize;

    return true;
}

bool SecFimgC110::m_DestroyG2D(void)
{
    if(m_g2dVirtAddr != NULL)
    {
        munmap(m_g2dVirtAddr, m_g2dSize);
        m_g2dVirtAddr = NULL;
        m_g2dPhysAddr = 0;
        m_g2dSize = 0;
    }

    if(0 < m_g2dFd)
        close(m_g2dFd);
    m_g2dFd = 0;

    return true;
}

//bool SecFimgC110::m_DoG2D(FimgRect * src, FimgRect * dst, int rotateValue, int alphaValue, int colorKey)
bool SecFimgC110::m_DoG2D(FimgRect * src, FimgRect * dst, FimgFlag * flag)
{
    g2d_params params;

    params.src_rect = src;
    params.dst_rect = dst;
    params.flag     = flag;

    if(ioctl(m_g2dFd, G2D_BLIT, &params) < 0)
    {
        PRINT("%s::G2D_BLIT fail\n", __func__);

        #if 0
        {
            PRINT("---------------------------------------\n");
            if(src)
            {
                PRINT("src.color_format : %d \n", src->color_format);
                PRINT("src.full_w       : %d \n", src->full_w);
                PRINT("src.full_h       : %d \n", src->full_h);
                PRINT("src.x            : %d \n", src->x);
                PRINT("src.y            : %d \n", src->y);
                PRINT("src.w            : %d \n", src->w);
                PRINT("src.h            : %d \n", src->h);
                PRINT("src.phys_addr    : %x \n", src->phys_addr);
            }
            else
                PRINT("flag.color_val   : %d \n", flag->color_val);

            PRINT("dst.color_format : %d \n", dst->color_format);
            PRINT("dst.full_w       : %d \n", dst->full_w);
            PRINT("dst.full_h       : %d \n", dst->full_h);
            PRINT("dst.x            : %d \n", dst->x);
            PRINT("dst.y            : %d \n", dst->y);
            PRINT("dst.w            : %d \n", dst->w);
            PRINT("dst.h            : %d \n", dst->h);
            PRINT("dst.phys_addr    : %x \n", dst->phys_addr);

            PRINT("flag.rotate_val  : %d \n", flag->rotate_val);
            PRINT("flag.alpha_val   : %d(%d) \n", flag->alpha_val);
            PRINT("flag.color_key_mode  : %d(%d) \n", flag->color_key_mode, flag->color_key_val);
            PRINT("---------------------------------------\n");
        }
        #endif

        return false;
    }

    return true;
}

inline bool SecFimgC110::m_PollG2D(struct pollfd * events)
{
    #define G2D_POLL_TIME (200)

    int ret;

    ret = poll(events, 1, G2D_POLL_TIME);

    if (ret < 0) {
        PRINT("%s::poll fail \n", __func__);
        return false;
    }
    else if (ret == 0) {
        PRINT("%s::No data in %d milli secs..\n", __func__, G2D_POLL_TIME);
        return false;
    }

    return true;
}

inline bool SecFimgC110::m_CleanG2D(unsigned int physAddr, unsigned int size)
{
    g2d_dma_info dma_info = { physAddr, size };

    if(ioctl(m_g2dFd, G2D_DMA_CACHE_CLEAN, &dma_info) < 0)
    {
        PRINT("%s::G2D_DMA_CACHE_CLEAN(%d, %d) fail\n", __func__, physAddr, size);
        return false;
    }
    return true;
}

inline bool SecFimgC110::m_InvalidG2D(unsigned int physAddr, unsigned int size)
{
    g2d_dma_info dma_info = { physAddr, size };

    if(ioctl(m_g2dFd, G2D_DMA_CACHE_INVAL, &dma_info) < 0)
    {
        PRINT("%s::G2D_DMA_CACHE_INVAL(%d, %d) fail\n", __func__, physAddr, size);
        return false;
    }
    return true;
}

inline bool SecFimgC110::m_FlushG2D  (unsigned int physAddr, unsigned int size)
{
    g2d_dma_info dma_info = { physAddr, size };

    if(ioctl(m_g2dFd, G2D_DMA_CACHE_FLUSH, &dma_info) < 0)
    {
        PRINT("%s::G2D_DMA_CACHE_FLUSH(%d, %d) fail\n", __func__, physAddr, size);
        return false;
    }
    return true;
}

inline int SecFimgC110::m_ColorFormatSecFimg2FimgHw(int color_format)
{
    // sw5771.park : The byte order is different between Android & Fimg.
    //                         [ 0][ 8][16][24][32]
    //               Android :     R   G   B   A
    //               Fimg    :     A   B   G   R

    switch (color_format)
    {
        case COLOR_FORMAT_RGB_565   : return G2D_RGB_565  ;

        case COLOR_FORMAT_RGBA_8888 : return G2D_RGBA_8888;
        case COLOR_FORMAT_ARGB_8888 : return G2D_ARGB_8888;
        case COLOR_FORMAT_BGRA_8888 : return G2D_BGRA_8888;
        case COLOR_FORMAT_ABGR_8888 : return G2D_ABGR_8888;

        case COLOR_FORMAT_RGBX_8888 : return G2D_RGBX_8888;
        case COLOR_FORMAT_XRGB_8888 : return G2D_XRGB_8888;
        case COLOR_FORMAT_BGRX_8888 : return G2D_BGRX_8888;
        case COLOR_FORMAT_XBGR_8888 : return G2D_XBGR_8888;

        case COLOR_FORMAT_RGBA_5551 : return G2D_RGBA_5551;
        case COLOR_FORMAT_ARGB_1555 : return G2D_ARGB_1555;
        case COLOR_FORMAT_BGRA_5551 : return G2D_BGRA_5551;
        case COLOR_FORMAT_ABGR_1555 : return G2D_ABGR_1555;

        case COLOR_FORMAT_RGBX_5551 : return G2D_RGBX_5551;
        case COLOR_FORMAT_XRGB_1555 : return G2D_XRGB_1555;
        case COLOR_FORMAT_BGRX_5551 : return G2D_BGRX_5551;
        case COLOR_FORMAT_XBGR_1555 : return G2D_XBGR_1555;

        case COLOR_FORMAT_RGBA_4444 : return G2D_RGBA_4444;
        case COLOR_FORMAT_ARGB_4444 : return G2D_ARGB_4444;
        case COLOR_FORMAT_BGRA_4444 : return G2D_BGRA_4444;
        case COLOR_FORMAT_ABGR_4444 : return G2D_ABGR_4444;

        case COLOR_FORMAT_RGBX_4444 : return G2D_RGBX_4444;
        case COLOR_FORMAT_XRGB_4444 : return G2D_XRGB_4444;
        case COLOR_FORMAT_BGRX_4444 : return G2D_BGRX_4444;
        case COLOR_FORMAT_XBGR_4444 : return G2D_XBGR_4444;

        default :
            PRINT("%s::not matched frame format : %d\n", __func__, color_format);
        break;
    }
    return -1;
}


inline int SecFimgC110::m_RotateValueSecFimg2FimgHw(int rotateValue)
{
    switch (rotateValue)
    {
        case ROTATE_0:      return G2D_ROT_0;
        case ROTATE_90:     return G2D_ROT_90;
        case ROTATE_180:    return G2D_ROT_180;
        case ROTATE_270:    return G2D_ROT_270;
        case ROTATE_X_FLIP: return G2D_ROT_X_FLIP;
        case ROTATE_Y_FLIP: return G2D_ROT_Y_FLIP;
    }

    return -1;
}

bool SecFimgC110::m_GetCalibratedNum(unsigned int * num1, unsigned int * num2)
{
    // sw5771.park : This is calibration for (X,1) and (1,X)
    //               This should be fixed on Device Driver..
    //               This info is VERY IMPORTANT..
    unsigned int w = *num1;
    unsigned int h = *num2;

    if(h == 1)
    {
        unsigned int mulNum = 0;

        #define NUM_OF_PRIMENUM (4)
        unsigned int primeNumList[NUM_OF_PRIMENUM] = { 2, 3, 5, 7 };

        for(int i = 0; i < NUM_OF_PRIMENUM; i++)
        {
            if((w % primeNumList[i]) == 0)
            {
                if(w == primeNumList[i])
                    break; // this can make another variable as 1.. so this is fail.

                mulNum = primeNumList[i];
                break;
            }
        }

        if(mulNum == 0)
        {
            // there is no calibration number..
            PRINTD("%s::calibration for (%d,%d) is fail\n", __func__, w, h);
            return false;
        }

        h = mulNum;

        if(mulNum == 2)
            w = (w >> 1);
        else
            w = (w / mulNum);
    }
    else if (w == 1)
    {
        unsigned int mulNum = 0;

        #define NUM_OF_PRIMENUM (4)
        unsigned int primeNumList[NUM_OF_PRIMENUM] = { 2, 3, 5, 7};

        for(int i = 0; i < NUM_OF_PRIMENUM; i++)
        {
            if((h % primeNumList[i]) == 0)
            {
                if(h == primeNumList[i])
                    break; // this can make another variable as 1.. so this is fail.

                mulNum = primeNumList[i];
                break;
            }
        }

        if(mulNum == 0)
        {
            // there is no calibration number..
            PRINT("%s::calibration for (X,1) and (1,X) is fail\n", __func__);
            return false;
        }

        w = mulNum;

        if(mulNum == 2)
            h = (h >> 1);
        else
            h = (h / mulNum);
    }

    *num1 = w;
    *num2 = h;

    return true;
}
#ifdef CHECK_FIMGC110_PERFORMANCE
void SecFimgC110::m_PrintSecFimgC110Performance(FimgRect *   src,
                                                FimgRect *   dst,
                                                int          stopWatchIndex,
                                                const char * stopWatchName[],
                                                nsecs_t      stopWatchTime[])
{
    char * srcColorFormat = "UNKNOW_COLOR_FORMAT";
    char * dstColorFormat = "UNKNOW_COLOR_FORMAT";

    switch(src->color_format)
    {
        case COLOR_FORMAT_RGB_565   :
            srcColorFormat = "RGB_565";
            break;
        case COLOR_FORMAT_RGBA_8888 :
            srcColorFormat = "RGBA_8888";
            break;
        case COLOR_FORMAT_RGBX_8888 :
            srcColorFormat = "RGBX_8888";
            break;
        default :
            srcColorFormat = "UNKNOW_COLOR_FORMAT";
            break;
    }

    switch(dst->color_format)
    {
        case COLOR_FORMAT_RGB_565   :
            dstColorFormat = "RGB_565";
            break;
        case COLOR_FORMAT_RGBA_8888 :
            dstColorFormat = "RGBA_8888";
            break;
        case COLOR_FORMAT_RGBX_8888 :
            dstColorFormat = "RGBX_8888";
            break;
        default :
            dstColorFormat = "UNKNOW_COLOR_FORMAT";
            break;
    }


    #ifdef CHECK_FIMGC110_CRITICAL_PERFORMANCE
    #else
        PRINT("===============================================\n");
        PRINT("src[%3d, %3d | %10s] -> dst[%3d, %3d | %10s]\n",
              src->w, src->h, srcColorFormat,
              dst->w, dst->h, dstColorFormat);
    #endif

    nsecs_t totalTime = stopWatchTime[stopWatchIndex - 1];

    for(int i = 0 ; i < stopWatchIndex; i++)
    {
        nsecs_t sectionTime;

        if(i != 0)
            sectionTime = stopWatchTime[i] - stopWatchTime[i-1];
        else
            sectionTime = stopWatchTime[i];

        #ifdef CHECK_FIMGC110_CRITICAL_PERFORMANCE
        if(1500 < (sectionTime / 1000)) // check 1.5 mille second..
        #endif
        {
            PRINT("===============================================\n");
            PRINT("src[%3d, %3d | %10s] -> dst[%3d, %3d | %10s]\n",
                   src->w, src->h, srcColorFormat,
                   dst->w, dst->h, dstColorFormat);

            PRINT("%20s : %5lld msec(%5.2f %%)\n",
              stopWatchName[i],
              sectionTime / 1000,
              ((float)sectionTime / (float)totalTime) * 100.0f);
        }
    }

}
#endif // CHECK_FIMGC110_PERFORMANCE

//---------------------------------------------------------------------------//
// extern function
//---------------------------------------------------------------------------//
extern "C" struct SecFimg * createSecFimg()
{
    if (secFimgAutoFreeThread == 0)
        secFimgAutoFreeThread = new SecFimgAutoFreeThread();
    else
        secFimgAutoFreeThread->SetOneMoreSleep();

    return SecFimgC110::CreateInstance();
}

extern "C" void destroySecFimg(SecFimg * ptrSecFimg)
{
    // Dont' call DestrotInstance..
    // return SecSecFimgC110::DestroyInstance(ptrSecFimg);
}

}; // namespace android
