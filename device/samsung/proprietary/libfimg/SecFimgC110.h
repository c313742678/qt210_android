/*
**
** Copyright 2008, The Android Open Source Project
** Copyright 2009 Samsung Electronics Co, Ltd. All Rights Reserved.
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
**
** @author Sangwoo, Park(sw5771.park@samsung.com)
** @date   2009-07-01
**
*/

#ifndef __SAMSUNG_SYSLSI_APDEV_FIMGC110LIB_H__
#define __SAMSUNG_SYSLSI_APDEV_FIMGC110LIB_H__


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/stat.h>

#include <linux/android_pmem.h>
#include <utils/threads.h>
#include <utils/StopWatch.h>

#include "SecFimg.h"

//-----------------------------------------------------------------//

namespace android
{

//#define CHECK_FIMGC110_PERFORMANCE
//#define CHECK_FIMGC110_CRITICAL_PERFORMANCE
#define NUMBER_FIMG_LIST           (1)  // kcoolsw : because of pmem
#define G2D_NONE_BLOCKING_MODE

//---------------------------------------------------------------------------//
// class SecFimgC110 : public FimgBase
//---------------------------------------------------------------------------//
class SecFimgC110 : public SecFimg
{
private :
    int              m_g2dFd;

    unsigned char *  m_g2dVirtAddr;
    unsigned int     m_g2dPhysAddr;
    unsigned int     m_g2dSize;
    unsigned char *  m_g2dSrcVirtAddr;
    unsigned int     m_g2dSrcPhysAddr;
    unsigned int     m_g2dSrcSize;
    unsigned char *  m_g2dDstVirtAddr;
    unsigned int     m_g2dDstPhysAddr;
    unsigned int     m_g2dDstSize;
    struct pollfd    m_g2dPoll;

    Mutex *          m_lock;

    static Mutex     m_instanceLock;
    static int       m_curSecFimgC110Index;
    static int       m_numOfInstance;

    static SecFimg * m_ptrSecFimgList[NUMBER_FIMG_LIST];


protected :
    SecFimgC110();
    virtual ~SecFimgC110();

public:
    static SecFimg * CreateInstance();
    static void      DestroyInstance(SecFimg * ptrSecFimg);
    static void      DestroyAllInstance(void);

protected:
    virtual bool     t_Create(void);
    virtual bool     t_Destroy(void);
    virtual bool     t_Stretch(FimgRect * src, FimgRect * dst, FimgFlag * flag);
    virtual bool     t_Lock(void);
    virtual bool     t_UnLock(void);

private:
    bool             m_CreateG2D(void);
    bool             m_DestroyG2D(void);

    bool             m_DoG2D(FimgRect * src, FimgRect * dst, FimgFlag * flag);

    inline bool      m_PollG2D(struct pollfd * events);

    inline bool      m_CleanG2D  (unsigned int physAddr, unsigned int size);
    inline bool      m_InvalidG2D(unsigned int physAddr, unsigned int size);
    inline bool      m_FlushG2D  (unsigned int physAddr, unsigned int size);

    inline int       m_ColorFormatSecFimg2FimgHw(int colorFormat);
    inline int       m_RotateValueSecFimg2FimgHw(int rotateValue);
    bool             m_GetCalibratedNum(unsigned int * num1, unsigned int * num2);

    #ifdef CHECK_FIMGC110_PERFORMANCE
    void             m_PrintSecFimgC110Performance(FimgRect *   src,
                                                FimgRect *   dst,
                                                int          stopWatchIndex,
                                                const char * stopWatchName[],
                                                nsecs_t      stopWatchTime[]);
    #endif // CHECK_FIMGC110_PERFORMANCE
};

//---------------------------------------------------------------------------//
// class SecFimgAutoFreeThread : public Thread
//---------------------------------------------------------------------------//
class SecFimgAutoFreeThread;

static sp<SecFimgAutoFreeThread> secFimgAutoFreeThread = 0;

class SecFimgAutoFreeThread : public Thread
{
    private:
        bool      mOneMoreSleep;
        bool      mDestroyed;

    public:
        SecFimgAutoFreeThread(void):
                    //Thread(true),
                    Thread(false),
                    mOneMoreSleep(true),
                    mDestroyed(false)
                    { }
        ~SecFimgAutoFreeThread(void)
        {
            if(mDestroyed == false)
            {
                SecFimgC110::DestroyAllInstance();
                mDestroyed = true;
            }
        }

        virtual void onFirstRef()
        {
            run("SecFimgAutoFreeThread", PRIORITY_BACKGROUND);
        }

        virtual bool threadLoop()
        {
            //#define SLEEP_TIME (10000000) // 10 sec
            #define SLEEP_TIME (3000000) // 3 sec
            //#define SLEEP_TIME (1000000) // 1 sec

            if(mOneMoreSleep == true)
            {
                mOneMoreSleep = false;
                usleep(SLEEP_TIME);

                return true;
            }
            else
            {
                if(mDestroyed == false)
                {
                    SecFimgC110::DestroyAllInstance();
                    mDestroyed = true;
                }

                secFimgAutoFreeThread = 0;

                return false;
            }
        }

        void SetOneMoreSleep(void)
        {
            mOneMoreSleep = true;
        }
};

}; // namespace android

#endif // __SAMSUNG_SYSLSI_APDEV_FIMGC110LIB_H__
