/*
**
** Copyright 2008, The Android Open Source Project
** Copyright 2010, Samsung Electronics Co. LTD
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

/*
**
** @author  Hyunkyung, Kim(hk310.kim@samsung.com)
** @date    2010-10-13
**
** @revised Sangwoo, Park(sw5771.park@samsung.com)
**          move to device folder
** @date    2010-12-25
*/

#define LOG_TAG "libhdmiservice"

#include "SecHdmiService.h"

namespace android {

static sp<ISurfaceComposer> g_surfaceFlinger = 0;

SecHdmiService::SecHdmiService()
{
    g_surfaceFlinger = m_getSurfaceFlinger();
}

SecHdmiService::~SecHdmiService()
{
}

SecHdmiService * SecHdmiService::getInstance(void)
{
    static SecHdmiService singleton;
    return &singleton;
}

void SecHdmiService::init()
{
    //LOGD("%s HDMI init", __func__);

    sp<ISurfaceComposer> surfaceFlinger= m_getSurfaceFlinger();
}

void SecHdmiService::setHdmiCableStatus(int status)
{
    //LOGD("%s HDMI status: %d", __func__, status);

    sp<ISurfaceComposer> surfaceFlinger= m_getSurfaceFlinger();
    if (surfaceFlinger != 0)
        surfaceFlinger->setHdmiStatus(status);
}

void SecHdmiService::setHdmiMode(int mode)
{
    //LOGD("%s HDMI Mode: %d", __func__, mode);

    sp<ISurfaceComposer> surfaceFlinger= m_getSurfaceFlinger();
    if (surfaceFlinger != 0)
        surfaceFlinger->setHdmiMode(mode);
}

void SecHdmiService::setHdmiResolution(int resolution)
{
    //LOGD("%s HDMI Resolution: %d", __func__, resolution);

    sp<ISurfaceComposer> surfaceFlinger= m_getSurfaceFlinger();
    if (surfaceFlinger != 0)
        surfaceFlinger->setHdmiResolution(resolution);
}

void SecHdmiService::setHdmiHdcp(int enHdcp)
{
    //LOGD("%s HDMI Hdcp: %d", __func__, enHdcp);

    sp<ISurfaceComposer> surfaceFlinger= m_getSurfaceFlinger();
    if (surfaceFlinger != 0)
        surfaceFlinger->setHdmiHdcp(enHdcp);
}

void SecHdmiService::blit2Hdmi(int w, int h,
                               int colorFormat,
                               int hdmiLayer,
                               unsigned int physYAddr,
                               unsigned int physCbAddr)
{
    // LOGD("%s HDMI blit: %d", __func__, hdmiLayer);

    sp<ISurfaceComposer> surfaceFlinger= m_getSurfaceFlinger();
    if (surfaceFlinger != 0) {
        surfaceFlinger->blit2Hdmi(w, h,
                                  colorFormat,
                                  hdmiLayer,
                                  physYAddr,
                                  physCbAddr);
    }
}

sp<ISurfaceComposer> SecHdmiService::m_getSurfaceFlinger(void)
{
    int ret = 0;

    if (g_surfaceFlinger == 0) {
        sp<IBinder> binder;
        sp<ISurfaceComposer> sc;
        sp<IServiceManager> sm = defaultServiceManager();
        int getSvcTimes = 0;
        for(getSvcTimes = 0; getSvcTimes < GETSERVICETIMEOUT; getSvcTimes++) {
            binder = sm->getService(String16("SurfaceFlinger"));
            if (binder == 0) {
                LOGW("SurfaceFlinger not published, waiting...");
                usleep(500000); // 0.5 s
            } else {
                break;
            }
        }
        // grab the lock again for updating g_surfaceFlinger
        if (getSvcTimes < GETSERVICETIMEOUT) {
            sc = interface_cast<ISurfaceComposer>(binder);
            g_surfaceFlinger = sc;
        } else {
            LOGW("Failed to get SurfaceFlinger... SecHdmiService will get it later..");
        }
    }
    return g_surfaceFlinger;
}

} /* namespace android */
