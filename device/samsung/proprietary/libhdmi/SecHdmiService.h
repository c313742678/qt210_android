/*
**
** Copyright 2008, The Android Open Source Project
** Copyright 2010, Samsung Electronics Co. LTD
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

/*
**
** @author  Hyunkyung, Kim(hk310.kim@samsung.com)
** @date    2010-10-13
**
** @revised Sangwoo, Park(sw5771.park@samsung.com)
**          move to device folder
** @date    2010-12-25
*/

#ifndef __SEC_HDMI_SERVICE_H__
#define __SEC_HDMI_SERVICE_H__

#include "utils/Log.h"

#include <linux/errno.h>
#include <stdint.h>
#include <sys/types.h>
#include <utils/RefBase.h>
#include <cutils/log.h>
#include <binder/IBinder.h>
#include <binder/IServiceManager.h>
#include <surfaceflinger/ISurfaceComposer.h>
#include <surfaceflinger/SurfaceComposerClient.h>

#define GETSERVICETIMEOUT (5)

namespace android {

class SecHdmiService
{
public:
    enum HDMI_MODE
    {
        HDMI_MODE_NONE = 0,
        HDMI_MODE_UI,
        HDMI_MODE_VIDEO,
    };
private:
    SecHdmiService();
    virtual ~SecHdmiService();

public:
        static SecHdmiService * getInstance(void);

        void init(void);
        void setHdmiCableStatus(int status);
        void setHdmiMode(int mode);
        void setHdmiResolution(int resolution);
        void setHdmiHdcp(int enHdcp);

        void blit2Hdmi(int w, int h,
                       int colorFormat,
                       int hdmiLayer,
                       unsigned int physYAddr,
                       unsigned int physCbAddr);

private:
        sp<ISurfaceComposer> m_getSurfaceFlinger(void);

};

}; // namespace android

#endif //__SEC_HDMI_SERVICE_H__
