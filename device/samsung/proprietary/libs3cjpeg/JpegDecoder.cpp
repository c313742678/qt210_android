/*
 * Copyright Samsung Electronics Co.,LTD.
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * JPEG DRIVER MODULE (JpegDecoder.cpp)
 * Author  : taikyung.yu@samsung.com
 * Date    : 05 Nov 2010
 * Purpose : This file implements the JPEG decoder APIs as needed by Camera HAL
 */
#define LOG_TAG "JpegDecoder"
#define MAIN_DUMP  0
#define THUMB_DUMP 0

#include <utils/Log.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "Jpeg.h"

namespace android {
JpegDecoder::JpegDecoder() : available(false)
{
    mArgs.mmapped_addr = (char *)MAP_FAILED;
    mArgs.dec_param       = NULL;
    mDevFd = 0;
}

JpegDecoder::~JpegDecoder()
{
}

bool JpegDecoder::create()
{
    mDevFd = open(JPG_DRIVER_NAME, O_RDWR);
    if (mDevFd < 0) {
        LOGE("Failed to open the device");
        return false;
    }

    mArgs.mmapped_addr = (char *)mmap(0,
                                      JPG_TOTAL_BUF_SIZE,
                                      PROT_READ | PROT_WRITE,
                                      MAP_SHARED,
                                      mDevFd,
                                      0);

    if (mArgs.mmapped_addr == MAP_FAILED) {
        LOGE("Failed to mmap");
        return false;
    }

    mArgs.dec_param = new jpg_dec_proc_param;
    if (mArgs.dec_param == NULL) {
        LOGE("Failed to allocate the memory for dec_param");
        return false;
    }
    memset(mArgs.dec_param, 0, sizeof(jpg_dec_proc_param));

    mArgs.dec_param->sample_mode = JPG_420;
    mArgs.dec_param->dec_type = JPG_MAIN;

    available = true;

    return true;
}

bool JpegDecoder::destroy()
{
    if (!available)
        return false;

    if (mArgs.mmapped_addr != (char*)MAP_FAILED)
        munmap(mArgs.mmapped_addr, JPG_TOTAL_BUF_SIZE);

    if (mArgs.dec_param)
        delete mArgs.dec_param;

    if (mDevFd > 0)
        close(mDevFd);

    available = false;

    return true;
}

jpg_return_status JpegDecoder::setConfig(jpeg_conf type, int32_t value)
{
    if (!available)
        return JPG_FAIL;

    jpg_return_status ret = JPG_SUCCESS;

    switch (type) {

    case JPEG_SET_DECODE_OUT_FORMAT:
        if (value != YCBCR_420 && value != YCBCR_422)
            ret = JPG_FAIL;
        else
            mArgs.dec_param->out_format = (out_mode_t)value;
        break;

    default:
        LOGE("Invalid Config type");
        ret = ERR_UNKNOWN;
    }

    if (ret == JPG_FAIL)
        LOGE("Invalid value(%d) for %d type", value, type);

    return ret;
}

jpg_return_status JpegDecoder::getConfig(jpeg_conf type, int32_t *value)
{
    if (!available)
        return JPG_FAIL;

    jpg_return_status ret = JPG_SUCCESS;

    switch (type) {
    case JPEG_GET_DECODE_WIDTH:
        *value = (int)mArgs.dec_param->width;
        break;

    case JPEG_GET_DECODE_HEIGHT:
        *value = (int)mArgs.dec_param->height;
        break;

    case JPEG_GET_SAMPING_MODE:
        *value = (int)mArgs.dec_param->sample_mode;
        break;

    default:
        LOGE("Invalid Config type");
        ret = ERR_UNKNOWN;
    }

    if (ret == JPG_FAIL)
        LOGE("Invalid value(%d) for %d type", value, type);

    return ret;
}

void* JpegDecoder::getInBuf(uint64_t size)
{
    if (!available)
        return NULL;

    if (size > JPG_STREAM_BUF_SIZE) {
        LOGE("The buffer size requested is too large");
        return NULL;
    }
    mArgs.dec_param->file_size = size;
    mArgs.in_buf = (char *)ioctl(mDevFd, IOCTL_JPG_GET_STRBUF, mArgs.mmapped_addr);
    return (void *)(mArgs.in_buf);
}

void* JpegDecoder::getOutBuf(uint64_t *size)
{
    if (!available)
        return NULL;

    if (mArgs.dec_param->data_size <= 0) {
        LOGE("The buffer requested doesn't have data");
        return NULL;
    }
    mArgs.out_buf = (char *)ioctl(mDevFd, IOCTL_JPG_GET_FRMBUF, mArgs.mmapped_addr);
    *size = mArgs.dec_param->data_size;
    return (void *)(mArgs.out_buf);
}

jpg_return_status JpegDecoder::decode()
{
    if (!available)
        return JPG_FAIL;

#ifdef JPEG_DEBUG
    LOGD("decode E");
#endif

    jpg_return_status ret = JPG_FAIL;

    jpg_dec_proc_param *param = mArgs.dec_param;

    if (param->dec_type == JPG_MAIN) {
        ret = (jpg_return_status)ioctl(mDevFd, IOCTL_JPG_DECODE, &mArgs);
        if (ret != JPG_SUCCESS)
            LOGE("Failed to decode main image");
    } else {
        // thumbnail decode, for the future work.
    }

#ifdef JPEG_DEBUG
        LOGD("decode X");
#endif

    return ret;
}

};
