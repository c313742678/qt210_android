/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/select.h>
#include <dlfcn.h>

#include "bma150.h"

#include <cutils/log.h>

#include "BmaSensor.h"

/*****************************************************************************/

int BmaSensor::bma_is_sensor_enabled(uint32_t sensor_type)
{
    int enabled = 0;

    if(dev_fd >= 0)
    {
        ioctl(dev_fd, BMA_IOCTL_IS_ENABLE, &enabled);
    }

    return enabled;
}

int BmaSensor::bma_enable_sensor(uint32_t sensor_type)
{
    if(dev_fd >= 0)
    {
        ioctl(dev_fd, BMA_IOCTL_ENABLE);
        return 0;
    }
    else
    {
        return -ENODEV;
    }
}

int BmaSensor::bma_disable_sensor(uint32_t sensor_type)
{
    if(dev_fd >= 0)
    {
        ioctl(dev_fd, BMA_IOCTL_DISABLE);

        return 0;
    }
    else
    {
        return -ENODEV;
    }
}

int BmaSensor::bma_set_delay(uint64_t delay)
{
    int delay_ms;
    if(dev_fd >= 0)
    {
        switch(delay)
        {
        case 200:
            delay_ms = SENSOR_DELAY_NORMAL;
            break;
        case 60:
            delay_ms = SENSOR_DELAY_UI;
            break;
        case 20:
            delay_ms = SENSOR_DELAY_GAME;
            break;
        case 0:
            delay_ms = SENSOR_DELAY_FASTEST;
            break;
        }

        ioctl(dev_fd, BMA_IOCTL_DELAY, &delay_ms);

        return 0;
    }
    else
    {
        return -ENODEV;
    }
}

BmaSensor::BmaSensor()
: SensorBase(NULL, NULL),
      mEnabled(0),
      mPendingMask(0),
      mInputReader(32)
{
    /* Open the library before opening the input device.  The library
     * creates a uinput device.
     */
    dev_name = BMA150_PATH;
    open_device();

    data_name = "bma150";
    data_fd = openInput("bma150");

    memset(mPendingEvents, 0, sizeof(mPendingEvents));

    mPendingEvents[Accelerometer].version = sizeof(sensors_event_t);
    mPendingEvents[Accelerometer].sensor = ID_A;
    mPendingEvents[Accelerometer].type = SENSOR_TYPE_ACCELEROMETER;
    mPendingEvents[Accelerometer].acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;

    for (int i=0 ; i<numSensors ; i++)
        mDelays[i] = 200000000; // 200 ms by default

    // read the actual value of all sensors if they're enabled already
    struct input_absinfo absinfo;
    short flags = 0;

	if (bma_is_sensor_enabled(SENSOR_TYPE_ACCELEROMETER))  {
        mEnabled |= 1<<Accelerometer;
        if (!ioctl(data_fd, EVIOCGABS(EVENT_TYPE_ACCEL_X), &absinfo)) {
            mPendingEvents[Accelerometer].acceleration.x = absinfo.value * CONVERT_A_X;
        }
        if (!ioctl(data_fd, EVIOCGABS(EVENT_TYPE_ACCEL_Y), &absinfo)) {
            mPendingEvents[Accelerometer].acceleration.y = absinfo.value * CONVERT_A_Y;
        }
        if (!ioctl(data_fd, EVIOCGABS(EVENT_TYPE_ACCEL_Z), &absinfo)) {
            mPendingEvents[Accelerometer].acceleration.z = absinfo.value * CONVERT_A_Z;
        }
    }
}

BmaSensor::~BmaSensor()
{
    close_device();
}

int BmaSensor::enable(int32_t handle, int en)
{
    int what = -1;

    switch (handle) {
        case ID_A: what = Accelerometer; break;
    }

    if (uint32_t(what) >= numSensors)
        return -EINVAL;

    int newState  = en ? 1 : 0;
    int err = 0;

    if ((uint32_t(newState)<<what) != (mEnabled & (1<<what))) {
        uint32_t sensor_type;
        switch (what) {
            case Accelerometer: sensor_type = SENSOR_TYPE_ACCELEROMETER;  break;
        }
        short flags = newState;
        if (en)
            err = bma_enable_sensor(sensor_type);
        else
            err = bma_disable_sensor(sensor_type);

        LOGE_IF(err, "Could not change sensor state (%s)", strerror(-err));
        if (!err) {
            mEnabled &= ~(1<<what);
            mEnabled |= (uint32_t(flags)<<what);
            update_delay();
        }
    }
    return err;
}

int BmaSensor::setDelay(int32_t handle, int64_t ns)
{
    int what = -1;
    switch (handle) {
        case ID_A: what = Accelerometer; break;
    }

    if (uint32_t(what) >= numSensors)
        return -EINVAL;

    if (ns < 0)
        return -EINVAL;

    mDelays[what] = ns;
    return update_delay();
}

int BmaSensor::update_delay()
{
    if (mEnabled) {
        uint64_t wanted = -1LLU;
        for (int i=0 ; i<numSensors ; i++) {
            if (mEnabled & (1<<i)) {
                uint64_t ns = mDelays[i];
                wanted = wanted < ns ? wanted : ns;
            }
        }
        return bma_set_delay(int64_t(wanted));
    }
    return 0;
}

int BmaSensor::readEvents(sensors_event_t* data, int count)
{
    if (count < 1)
        return -EINVAL;

    ssize_t n = mInputReader.fill(data_fd);
    if (n < 0)
        return n;

    int numEventReceived = 0;
    input_event const* event;

    while (count && mInputReader.readEvent(&event)) {
        int type = event->type;
        if (type == EV_ABS) {
            processEvent(event->code, event->value);
            mInputReader.next();
        } else if (type == EV_SYN) {
            int64_t time = timevalToNano(event->time);
            for (int j=0 ; count && mPendingMask && j<numSensors ; j++) {
                if (mPendingMask & (1<<j)) {
                    mPendingMask &= ~(1<<j);
                    mPendingEvents[j].timestamp = time;
                    if (mEnabled & (1<<j)) {
                        *data++ = mPendingEvents[j];
                        count--;
                        numEventReceived++;
                    }
                }
            }
//	    LOGI("BmaSensor data : EV_SYN %f, %f, %f",
//				 mPendingEvents[Accelerometer].acceleration.x,
//				 mPendingEvents[Accelerometer].acceleration.y,
//				 mPendingEvents[Accelerometer].acceleration.z);
            if (!mPendingMask) {
                mInputReader.next();
            }
        } else {
            LOGE("BmaSensor: unknown event (type=%d, code=%d)",
                    type, event->code);
            mInputReader.next();
        }
    }
    return numEventReceived;
}

void BmaSensor::processEvent(int code, int value)
{
    switch (code) {
        case EVENT_TYPE_ACCEL_X:
            mPendingMask |= 1<<Accelerometer;
            mPendingEvents[Accelerometer].acceleration.x = value * CONVERT_A_X;
            break;
        case EVENT_TYPE_ACCEL_Y:
            mPendingMask |= 1<<Accelerometer;
            mPendingEvents[Accelerometer].acceleration.y = value * CONVERT_A_Y;
            break;
        case EVENT_TYPE_ACCEL_Z:
            mPendingMask |= 1<<Accelerometer;
            mPendingEvents[Accelerometer].acceleration.z = value * CONVERT_A_Z;
            break;
    }
//    LOGI("BmaSensor data : ProcessEvent %f, %f, %f",
//                                 mPendingEvents[Accelerometer].acceleration.x,
//                                 mPendingEvents[Accelerometer].acceleration.y,
//                                 mPendingEvents[Accelerometer].acceleration.z);

}
