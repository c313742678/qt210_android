/*
 * Definitions for bma150 accelate chip.
 */
#ifndef BMA150_H
#define BMA150_H

#include <linux/ioctl.h>

#define BMAIO			0x50
#define BMA150_PATH             "/dev/bma150"

/* IOCTLs for BMA library */
#define BMA_IOCTL_ENABLE	_IO(BMAIO, 0x31)
#define BMA_IOCTL_DISABLE	_IO(BMAIO, 0x32)
#define BMA_IOCTL_IS_ENABLE	_IOR(BMAIO, 0x33, int)
#define BMA_IOCTL_DELAY		_IOW(BMAIO, 0x35, int)

/* IOCTLs for APPs */
#define SENSOR_DELAY_FASTEST    0       // delay = 10
#define SENSOR_DELAY_GAME       1       // delay = 20
#define SENSOR_DELAY_UI         2       // delay = 60
#define SENSOR_DELAY_NORMAL     3       // delay = 200

#endif
