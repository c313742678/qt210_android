ifeq ($(filter-out s5pc110,$(TARGET_BOARD_PLATFORM)),)

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    stagefright_overlay_output.cpp \
    SecHardwareRenderer.cpp \
    SEC_OMX_Plugin.cpp

LOCAL_CFLAGS += $(PV_CFLAGS_MINUS_VISIBILITY)
LOCAL_CFLAGS += -DZERO_COPY

ifeq ($(BOARD_USES_OVERLAY), true)
		LOCAL_CFLAGS += -DBOARD_USES_OVERLAY
endif

ifeq ($(BOARD_USES_COPYBIT),true)
LOCAL_CFLAGS += -DBOARD_USES_COPYBIT
endif

LOCAL_C_INCLUDES:= \
      $(TOP)/frameworks/base/include/media/stagefright/openmax \
      $(TOP)/external/opencore/extern_libs_v2/khronos/openmax/include \
      $(LOCAL_PATH)/../include \
      $(LOCAL_PATH)/../liboverlay

LOCAL_SHARED_LIBRARIES :=    \
        libbinder            \
        libutils             \
        libcutils            \
        libui                \
        libdl                \
        libsurfaceflinger_client

LOCAL_MODULE := libstagefrighthw

ifeq ($(BOARD_USE_SAMSUNG_COLORFORMAT), true)
LOCAL_CFLAGS += -DUSE_SAMSUNG_COLORFORMAT
endif

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)

endif

