# Copyright (C) 2007 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# BoardConfig.mk
#
# Product-specific compile-time definitions.
#

# Set this up here so that BoardVendorConfig.mk can override it
BOARD_USES_GENERIC_AUDIO := false

BOARD_USES_LIBSECRIL_STUB := true

BOARD_NO_PAGE_FLIPPING := false

# hcj modified
# BusyBox & Bash Shell
BOARD_USE_BUSYBOX := true
ifeq ($(BOARD_USE_BUSYBOX),true)
BUSYBOX_EXT_COMPILE := arm-none-linux-gnueabi-
endif

# Use the non-open-source parts, if they're present
-include vendor/samsung/smdkv210/BoardConfigVendor.mk

TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi

BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_BCM := true

TARGET_NO_BOOTLOADER := true

TARGET_NO_KERNEL := true

TARGET_NO_RADIOIMAGE := true
TARGET_PROVIDES_INIT_RC := true
TARGET_PROVIDES_INIT_TARGET_RC := true
TARGET_BOARD_PLATFORM := s5pc110
TARGET_BOOTLOADER_BOARD_NAME := smdkv210

TARGET_SEC_INTERNAL_STORAGE := false

# Enable NEON feature
TARGET_ARCH_VARIANT := armv7-a-neon
ARCH_ARM_HAVE_TLS_REGISTER := true

USE_CAMERA_STUB := false
ifeq ($(USE_CAMERA_STUB),false)
BOARD_CAMERA_LIBRARIES := libcamera
endif

BOARD_USES_DUAL_CAMERA := false
BOARD_USES_HGL := true
BOARD_USES_OVERLAY := true
BOARD_USES_COPYBIT := true

# hcj modified
# Sensor board
BOARD_USE_QT210_SENSOR := true
# WIFI board
BOARD_USES_REALTEK_WIFI := true
#BOARD_USE_MANGO_LIGHT := true

BOARD_USES_HDMI :=true
BOARD_USES_HDMI_SUBTITLES := false
ifeq ($(BOARD_USES_HDMI_SUBTITLES),true)
BOARD_USES_FIMGAPI := true
endif

DEFAULT_FB_NUM := 2
#hcj if we use small page, just comment this out
#BOARD_NAND_PAGE_SIZE := 4096 -s 128

BOARD_KERNEL_BASE := 0x30000000
BOARD_KERNEL_PAGESIZE := 4096
#BOARD_KERNEL_CMDLINE := console=ttyFIQ0 no_console_suspend
BOARD_KERNEL_CMDLINE := console=ttySAC0 no_console_suspend

TARGET_RECOVERY_UI_LIB := librecovery_ui_smdkv210
TARGET_RELEASETOOLS_EXTENSIONS := device/samsung/smdkv210

BOARD_SDMMC_BSP := false

ifeq ($(BOARD_SDMMC_BSP),true)
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 125829120
BOARD_USERDATAIMAGE_PARTITION_SIZE := 370147328
BOARD_FLASH_BLOCK_SIZE := 4096
endif

BOARD_USE_SAMSUNG_COLORFORMAT := true

# Wi-Fi
#BOARD_HAVE_LIBWIFI := true
#BOARD_WPA_SUPPLICANT_DRIVER := CUSTOM
#WPA_BUILD_SUPPLICANT := true
#WPA_SUPPLICANT_VERSION := VER_0_5_X
#CONFIG_CTRL_IFACE := y

BOARD_HAVE_LIBWIFI := true
BOARD_WPA_SUPPLICANT_DRIVER := WEXT
WPA_SUPPLICANT_VERSION      := VER_0_5_X
#BOARD_WLAN_DEVICE           := 8192cu
#WIFI_DRIVER_MODULE_PATH     := "/system/lib/modules/8192cu.ko"
#WIFI_DRIVER_MODULE_NAME     := "8192cu"
CONFIG_CTRL_IFACE := y

