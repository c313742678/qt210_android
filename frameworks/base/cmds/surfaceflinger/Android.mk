LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	main_surfaceflinger.cpp 

LOCAL_SHARED_LIBRARIES := \
	libsurfaceflinger \
	libbinder \
	libutils

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/../../services/surfaceflinger

ifeq ($(BOARD_USES_HDMI),true)
       LOCAL_SHARED_LIBRARIES += libhdmi libfimc
       LOCAL_CFLAGS     += -DBOARD_USES_HDMI

       LOCAL_C_INCLUDES += device/samsung/proprietary/include
       LOCAL_C_INCLUDES += device/samsung/proprietary/libhdmi
       LOCAL_C_INCLUDES += device/samsung/proprietary/libfimc
endif

LOCAL_MODULE:= surfaceflinger

include $(BUILD_EXECUTABLE)
