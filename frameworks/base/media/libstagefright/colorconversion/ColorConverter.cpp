/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <media/stagefright/ColorConverter.h>
#include <media/stagefright/MediaDebug.h>

namespace android {

static const int OMX_QCOM_COLOR_FormatYVU420SemiPlanar = 0x7FA30C00;

#ifdef USE_SAMSUNG_COLORFORMAT
static const int OMX_SEC_COLOR_FormatNV12TPhysicalAddress = 0x7F000001;
#endif

ColorConverter::ColorConverter(
        OMX_COLOR_FORMATTYPE from, OMX_COLOR_FORMATTYPE to)
    : mSrcFormat(from),
      mDstFormat(to),
      mClip(NULL) {
}

ColorConverter::~ColorConverter() {
    delete[] mClip;
    mClip = NULL;
}

bool ColorConverter::isValid() const {
    if (mDstFormat != OMX_COLOR_Format16bitRGB565) {
        return false;
    }

    switch (mSrcFormat) {
        case OMX_COLOR_FormatYUV420Planar:
        case OMX_COLOR_FormatCbYCrY:
        case OMX_QCOM_COLOR_FormatYVU420SemiPlanar:
#ifdef USE_SAMSUNG_COLORFORMAT
        case OMX_SEC_COLOR_FormatNV12TPhysicalAddress:
#endif
        case OMX_COLOR_FormatYUV420SemiPlanar:
            return true;

        default:
            return false;
    }
}

void ColorConverter::convert(
        size_t width, size_t height,
        const void *srcBits, size_t srcSkip,
        void *dstBits, size_t dstSkip) {
    CHECK_EQ(mDstFormat, OMX_COLOR_Format16bitRGB565);

    switch (mSrcFormat) {
        case OMX_COLOR_FormatYUV420Planar:
            convertYUV420Planar(
                    width, height, srcBits, srcSkip, dstBits, dstSkip);
            break;

        case OMX_COLOR_FormatCbYCrY:
            convertCbYCrY(
                    width, height, srcBits, srcSkip, dstBits, dstSkip);
            break;

        case OMX_QCOM_COLOR_FormatYVU420SemiPlanar:
            convertQCOMYUV420SemiPlanar(
                    width, height, srcBits, srcSkip, dstBits, dstSkip);
            break;

#ifdef USE_SAMSUNG_COLORFORMAT
        case OMX_SEC_COLOR_FormatNV12TPhysicalAddress:
            convertSECNV12Tile(
                width, height, srcBits, srcSkip, dstBits, dstSkip);
            break;
#endif

        case OMX_COLOR_FormatYUV420SemiPlanar:
            convertYUV420SemiPlanar(
                    width, height, srcBits, srcSkip, dstBits, dstSkip);
            break;

        default:
        {
            CHECK(!"Should not be here. Unknown color conversion.");
            break;
        }
    }
}

void ColorConverter::convertCbYCrY(
        size_t width, size_t height,
        const void *srcBits, size_t srcSkip,
        void *dstBits, size_t dstSkip) {
    CHECK_EQ(srcSkip, 0);  // Doesn't really make sense for YUV formats.
    CHECK(dstSkip >= width * 2);
    CHECK((dstSkip & 3) == 0);

    uint8_t *kAdjustedClip = initClip();

    uint32_t *dst_ptr = (uint32_t *)dstBits;

    const uint8_t *src = (const uint8_t *)srcBits;

    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; x += 2) {
            signed y1 = (signed)src[2 * x + 1] - 16;
            signed y2 = (signed)src[2 * x + 3] - 16;
            signed u = (signed)src[2 * x] - 128;
            signed v = (signed)src[2 * x + 2] - 128;

            signed u_b = u * 517;
            signed u_g = -u * 100;
            signed v_g = -v * 208;
            signed v_r = v * 409;

            signed tmp1 = y1 * 298;
            signed b1 = (tmp1 + u_b) / 256;
            signed g1 = (tmp1 + v_g + u_g) / 256;
            signed r1 = (tmp1 + v_r) / 256;

            signed tmp2 = y2 * 298;
            signed b2 = (tmp2 + u_b) / 256;
            signed g2 = (tmp2 + v_g + u_g) / 256;
            signed r2 = (tmp2 + v_r) / 256;

            uint32_t rgb1 =
                ((kAdjustedClip[r1] >> 3) << 11)
                | ((kAdjustedClip[g1] >> 2) << 5)
                | (kAdjustedClip[b1] >> 3);

            uint32_t rgb2 =
                ((kAdjustedClip[r2] >> 3) << 11)
                | ((kAdjustedClip[g2] >> 2) << 5)
                | (kAdjustedClip[b2] >> 3);

            dst_ptr[x / 2] = (rgb2 << 16) | rgb1;
        }

        src += width * 2;
        dst_ptr += dstSkip / 4;
    }
}

void ColorConverter::convertYUV420Planar(
        size_t width, size_t height,
        const void *srcBits, size_t srcSkip,
        void *dstBits, size_t dstSkip) {
    CHECK_EQ(srcSkip, 0);  // Doesn't really make sense for YUV formats.
    CHECK(dstSkip >= width * 2);
    CHECK((dstSkip & 3) == 0);

    uint8_t *kAdjustedClip = initClip();

    uint32_t *dst_ptr = (uint32_t *)dstBits;
    const uint8_t *src_y = (const uint8_t *)srcBits;

    const uint8_t *src_u =
        (const uint8_t *)src_y + width * height;

    const uint8_t *src_v =
        (const uint8_t *)src_u + (width / 2) * (height / 2);

    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; x += 2) {
            // B = 1.164 * (Y - 16) + 2.018 * (U - 128)
            // G = 1.164 * (Y - 16) - 0.813 * (V - 128) - 0.391 * (U - 128)
            // R = 1.164 * (Y - 16) + 1.596 * (V - 128)

            // B = 298/256 * (Y - 16) + 517/256 * (U - 128)
            // G = .................. - 208/256 * (V - 128) - 100/256 * (U - 128)
            // R = .................. + 409/256 * (V - 128)

            // min_B = (298 * (- 16) + 517 * (- 128)) / 256 = -277
            // min_G = (298 * (- 16) - 208 * (255 - 128) - 100 * (255 - 128)) / 256 = -172
            // min_R = (298 * (- 16) + 409 * (- 128)) / 256 = -223

            // max_B = (298 * (255 - 16) + 517 * (255 - 128)) / 256 = 534
            // max_G = (298 * (255 - 16) - 208 * (- 128) - 100 * (- 128)) / 256 = 432
            // max_R = (298 * (255 - 16) + 409 * (255 - 128)) / 256 = 481

            // clip range -278 .. 535

            signed y1 = (signed)src_y[x] - 16;
            signed y2 = (signed)src_y[x + 1] - 16;

            signed u = (signed)src_u[x / 2] - 128;
            signed v = (signed)src_v[x / 2] - 128;

            signed u_b = u * 517;
            signed u_g = -u * 100;
            signed v_g = -v * 208;
            signed v_r = v * 409;

            signed tmp1 = y1 * 298;
            signed b1 = (tmp1 + u_b) / 256;
            signed g1 = (tmp1 + v_g + u_g) / 256;
            signed r1 = (tmp1 + v_r) / 256;

            signed tmp2 = y2 * 298;
            signed b2 = (tmp2 + u_b) / 256;
            signed g2 = (tmp2 + v_g + u_g) / 256;
            signed r2 = (tmp2 + v_r) / 256;

            uint32_t rgb1 =
                ((kAdjustedClip[r1] >> 3) << 11)
                | ((kAdjustedClip[g1] >> 2) << 5)
                | (kAdjustedClip[b1] >> 3);

            uint32_t rgb2 =
                ((kAdjustedClip[r2] >> 3) << 11)
                | ((kAdjustedClip[g2] >> 2) << 5)
                | (kAdjustedClip[b2] >> 3);

            dst_ptr[x / 2] = (rgb2 << 16) | rgb1;
        }

        src_y += width;

        if (y & 1) {
            src_u += width / 2;
            src_v += width / 2;
        }

        dst_ptr += dstSkip / 4;
    }
}

void ColorConverter::convertQCOMYUV420SemiPlanar(
        size_t width, size_t height,
        const void *srcBits, size_t srcSkip,
        void *dstBits, size_t dstSkip) {
    CHECK_EQ(srcSkip, 0);  // Doesn't really make sense for YUV formats.
    CHECK(dstSkip >= width * 2);
    CHECK((dstSkip & 3) == 0);

    uint8_t *kAdjustedClip = initClip();

    uint32_t *dst_ptr = (uint32_t *)dstBits;
    const uint8_t *src_y = (const uint8_t *)srcBits;

    const uint8_t *src_u =
        (const uint8_t *)src_y + width * height;

    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; x += 2) {
            signed y1 = (signed)src_y[x] - 16;
            signed y2 = (signed)src_y[x + 1] - 16;

            signed u = (signed)src_u[x & ~1] - 128;
            signed v = (signed)src_u[(x & ~1) + 1] - 128;

            signed u_b = u * 517;
            signed u_g = -u * 100;
            signed v_g = -v * 208;
            signed v_r = v * 409;

            signed tmp1 = y1 * 298;
            signed b1 = (tmp1 + u_b) / 256;
            signed g1 = (tmp1 + v_g + u_g) / 256;
            signed r1 = (tmp1 + v_r) / 256;

            signed tmp2 = y2 * 298;
            signed b2 = (tmp2 + u_b) / 256;
            signed g2 = (tmp2 + v_g + u_g) / 256;
            signed r2 = (tmp2 + v_r) / 256;

            uint32_t rgb1 =
                ((kAdjustedClip[b1] >> 3) << 11)
                | ((kAdjustedClip[g1] >> 2) << 5)
                | (kAdjustedClip[r1] >> 3);

            uint32_t rgb2 =
                ((kAdjustedClip[b2] >> 3) << 11)
                | ((kAdjustedClip[g2] >> 2) << 5)
                | (kAdjustedClip[r2] >> 3);

            dst_ptr[x / 2] = (rgb2 << 16) | rgb1;
        }

        src_y += width;

        if (y & 1) {
            src_u += width;
        }

        dst_ptr += dstSkip / 4;
    }
}

void ColorConverter::convertYUV420SemiPlanar(
        size_t width, size_t height,
        const void *srcBits, size_t srcSkip,
        void *dstBits, size_t dstSkip) {
    CHECK_EQ(srcSkip, 0);  // Doesn't really make sense for YUV formats.
    CHECK(dstSkip >= width * 2);
    CHECK((dstSkip & 3) == 0);

    uint8_t *kAdjustedClip = initClip();

    uint32_t *dst_ptr = (uint32_t *)dstBits;
    const uint8_t *src_y = (const uint8_t *)srcBits;

    const uint8_t *src_u =
        (const uint8_t *)src_y + width * height;

    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; x += 2) {
            signed y1 = (signed)src_y[x] - 16;
            signed y2 = (signed)src_y[x + 1] - 16;

            signed v = (signed)src_u[x & ~1] - 128;
            signed u = (signed)src_u[(x & ~1) + 1] - 128;

            signed u_b = u * 517;
            signed u_g = -u * 100;
            signed v_g = -v * 208;
            signed v_r = v * 409;

            signed tmp1 = y1 * 298;
            signed b1 = (tmp1 + u_b) / 256;
            signed g1 = (tmp1 + v_g + u_g) / 256;
            signed r1 = (tmp1 + v_r) / 256;

            signed tmp2 = y2 * 298;
            signed b2 = (tmp2 + u_b) / 256;
            signed g2 = (tmp2 + v_g + u_g) / 256;
            signed r2 = (tmp2 + v_r) / 256;

            uint32_t rgb1 =
                ((kAdjustedClip[b1] >> 3) << 11)
                | ((kAdjustedClip[g1] >> 2) << 5)
                | (kAdjustedClip[r1] >> 3);

            uint32_t rgb2 =
                ((kAdjustedClip[b2] >> 3) << 11)
                | ((kAdjustedClip[g2] >> 2) << 5)
                | (kAdjustedClip[r2] >> 3);

            dst_ptr[x / 2] = (rgb2 << 16) | rgb1;
        }

        src_y += width;

        if (y & 1) {
            src_u += width;
        }

        dst_ptr += dstSkip / 4;
    }
}

uint8_t *ColorConverter::initClip() {
    static const signed kClipMin = -278;
    static const signed kClipMax = 535;

    if (mClip == NULL) {
        mClip = new uint8_t[kClipMax - kClipMin + 1];

        for (signed i = kClipMin; i <= kClipMax; ++i) {
            mClip[i - kClipMin] = (i < 0) ? 0 : (i > 255) ? 255 : (uint8_t)i;
        }
    }

    return &mClip[-kClipMin];
}

/* test converter SeungBeom Kim */
void ColorConverter::convertSECNV12Tile(
        size_t width, size_t height,
        const void *srcBits, size_t srcSkip,
        void *dstBits, size_t dstSkip)
{
    int i = 0;
    int32_t width_16, height_16;
    int32_t width_128, height_64;

    width_128 = ((width + 127) / 128) * 128;
    height_64 = ((height + 63) / 64) * 64;
    width_16 = ((width + 15) / 16) * 16;
    height_16 = ((height + 15) / 16) * 16;

    uint8_t *tmpBuffer_YUV = (uint8_t *)malloc((width_128 * height_64 * 3) / 2);
    uint8_t *tmpBuffer_RGB = (uint8_t *)malloc(width_128 * height_64 * 2);

    convertNV12tToYUV420(tmpBuffer_YUV, (uint8_t *)srcBits, width, height);
    convertYUV420Planar(width_16, height_16, tmpBuffer_YUV, srcSkip, tmpBuffer_RGB, width_16 * 2);

    for (i = 0; i < height; i++) {
        memcpy(dstBits + (width * i * 2), tmpBuffer_RGB + (width_16 * i * 2), width * 2);
    }
}

void ColorConverter::convertNV12tToYUV420(
        uint8_t *pDst, uint8_t *pSrc,
        int32_t videoWidth, int32_t videoHeight)
{
    int32_t width_16, height_16;
    int32_t width_128, height_64;

    width_16 = ((videoWidth + 15) / 16) * 16;
    height_16 = ((videoHeight + 15) / 16) * 16;
    width_128 = ((videoWidth + 127) / 128) * 128;
    height_64 = ((videoHeight + 63) / 64) * 64;

    tile_to_linear_64x32_4x2_neon(
        (unsigned char *)pDst,
        (unsigned char *)pSrc,
        width_16,
        height_16);
    tile_to_linear_64x32_4x2_uv_neon(
        (unsigned char *)pDst + (width_16 * height_16),
        (unsigned char *)pSrc + (width_128 * height_64),
        width_16,
        height_16 >> 1);
}

int ColorConverter::tile_4x2_read(int x_size, int y_size, int x_pos, int y_pos)
{
    int pixel_x_m1, pixel_y_m1;
    int roundup_x, roundup_y;
    int linear_addr0, linear_addr1, bank_addr ;
    int x_addr;
    int trans_addr;

    pixel_x_m1 = x_size -1;
    pixel_y_m1 = y_size -1;

    roundup_x = ((pixel_x_m1 >> 7) + 1);
    roundup_y = ((pixel_x_m1 >> 6) + 1);

    x_addr = x_pos >> 2;

    if ((y_size <= y_pos+32) && ( y_pos < y_size) &&
        (((pixel_y_m1 >> 5) & 0x1) == 0) && (((y_pos >> 5) & 0x1) == 0)) {
        linear_addr0 = (((y_pos & 0x1f) <<4) | (x_addr & 0xf));
        linear_addr1 = (((y_pos >> 6) & 0xff) * roundup_x + ((x_addr >> 6) & 0x3f));

        if (((x_addr >> 5) & 0x1) == ((y_pos >> 5) & 0x1))
            bank_addr = ((x_addr >> 4) & 0x1);
        else
            bank_addr = 0x2 | ((x_addr >> 4) & 0x1);
    } else {
        linear_addr0 = (((y_pos & 0x1f) << 4) | (x_addr & 0xf));
        linear_addr1 = (((y_pos >> 6) & 0xff) * roundup_x + ((x_addr >> 5) & 0x7f));

        if (((x_addr >> 5) & 0x1) == ((y_pos >> 5) & 0x1))
            bank_addr = ((x_addr >> 4) & 0x1);
        else
            bank_addr = 0x2 | ((x_addr >> 4) & 0x1);
    }

    linear_addr0 = linear_addr0 << 2;
    trans_addr = (linear_addr1 <<13) | (bank_addr << 11) | linear_addr0;

    return trans_addr;
}

void ColorConverter::Y_tile_to_linear_4x2(
        unsigned char *p_linear_addr, unsigned char *p_tiled_addr,
        unsigned int x_size, unsigned int y_size)
{
    int trans_addr;
    unsigned int i, j, k, index;
    unsigned char data8[4];
    unsigned int max_index = x_size * y_size;

    for (i = 0; i < y_size; i = i + 16) {
        for (j = 0; j < x_size; j = j + 16) {
            trans_addr = tile_4x2_read(x_size, y_size, j, i);
            for (k = 0; k < 16; k++) {
                /* limit check - prohibit segmentation fault */
                index = (i * x_size) + (x_size * k) + j;
                /* remove equal condition to solve thumbnail bug */
                if (index + 16 > max_index) {
                    continue;
                }

                data8[0] = p_tiled_addr[trans_addr + 64 * k + 0];
                data8[1] = p_tiled_addr[trans_addr + 64 * k + 1];
                data8[2] = p_tiled_addr[trans_addr + 64 * k + 2];
                data8[3] = p_tiled_addr[trans_addr + 64 * k + 3];

                p_linear_addr[index] = data8[0];
                p_linear_addr[index + 1] = data8[1];
                p_linear_addr[index + 2] = data8[2];
                p_linear_addr[index + 3] = data8[3];

                data8[0] = p_tiled_addr[trans_addr + 64 * k + 4];
                data8[1] = p_tiled_addr[trans_addr + 64 * k + 5];
                data8[2] = p_tiled_addr[trans_addr + 64 * k + 6];
                data8[3] = p_tiled_addr[trans_addr + 64 * k + 7];

                p_linear_addr[index + 4] = data8[0];
                p_linear_addr[index + 5] = data8[1];
                p_linear_addr[index + 6] = data8[2];
                p_linear_addr[index + 7] = data8[3];

                data8[0] = p_tiled_addr[trans_addr + 64 * k + 8];
                data8[1] = p_tiled_addr[trans_addr + 64 * k + 9];
                data8[2] = p_tiled_addr[trans_addr + 64 * k + 10];
                data8[3] = p_tiled_addr[trans_addr + 64 * k + 11];

                p_linear_addr[index + 8] = data8[0];
                p_linear_addr[index + 9] = data8[1];
                p_linear_addr[index + 10] = data8[2];
                p_linear_addr[index + 11] = data8[3];

                data8[0] = p_tiled_addr[trans_addr + 64 * k + 12];
                data8[1] = p_tiled_addr[trans_addr + 64 * k + 13];
                data8[2] = p_tiled_addr[trans_addr + 64 * k + 14];
                data8[3] = p_tiled_addr[trans_addr + 64 * k + 15];

                p_linear_addr[index + 12] = data8[0];
                p_linear_addr[index + 13] = data8[1];
                p_linear_addr[index + 14] = data8[2];
                p_linear_addr[index + 15] = data8[3];
            }
        }
    }
}

void ColorConverter::CbCr_tile_to_linear_4x2(
        unsigned char *p_linear_addr, unsigned char *p_tiled_addr,
        unsigned int x_size, unsigned int y_size)
{
    int trans_addr;
    unsigned int i, j, k, index;
    unsigned char data8[4];
    unsigned int half_y_size = y_size / 2;
    unsigned int max_index = x_size * half_y_size;
    unsigned char *pUVAddr[2];

    pUVAddr[0] = p_linear_addr;
    pUVAddr[1] = p_linear_addr + ((x_size * half_y_size) / 2);

    for (i = 0; i < half_y_size; i = i + 16) {
        for (j = 0; j < x_size; j = j + 16) {
            trans_addr = tile_4x2_read(x_size, half_y_size, j, i);
            for (k = 0; k < 16; k++) {
                /* limit check - prohibit segmentation fault */
                index = (i * x_size) + (x_size * k) + j;
                /* remove equal condition to solve thumbnail bug */
                if (index + 16 > max_index) {
                    continue;
                }

				data8[0] = p_tiled_addr[trans_addr + 64 * k + 0];
				data8[1] = p_tiled_addr[trans_addr + 64 * k + 1];
				data8[2] = p_tiled_addr[trans_addr + 64 * k + 2];
				data8[3] = p_tiled_addr[trans_addr + 64 * k + 3];

				pUVAddr[index%2][index/2] = data8[0];
				pUVAddr[(index+1)%2][(index+1)/2] = data8[1];
				pUVAddr[(index+2)%2][(index+2)/2] = data8[2];
				pUVAddr[(index+3)%2][(index+3)/2] = data8[3];

				data8[0] = p_tiled_addr[trans_addr + 64 * k + 4];
				data8[1] = p_tiled_addr[trans_addr + 64 * k + 5];
				data8[2] = p_tiled_addr[trans_addr + 64 * k + 6];
				data8[3] = p_tiled_addr[trans_addr + 64 * k + 7];

				pUVAddr[(index+4)%2][(index+4)/2] = data8[0];
				pUVAddr[(index+5)%2][(index+5)/2] = data8[1];
				pUVAddr[(index+6)%2][(index+6)/2] = data8[2];
				pUVAddr[(index+7)%2][(index+7)/2] = data8[3];

				data8[0] = p_tiled_addr[trans_addr + 64 * k + 8];
				data8[1] = p_tiled_addr[trans_addr + 64 * k + 9];
				data8[2] = p_tiled_addr[trans_addr + 64 * k + 10];
				data8[3] = p_tiled_addr[trans_addr + 64 * k + 11];

				pUVAddr[(index+8)%2][(index+8)/2] = data8[0];
				pUVAddr[(index+9)%2][(index+9)/2] = data8[1];
				pUVAddr[(index+10)%2][(index+10)/2] = data8[2];
				pUVAddr[(index+11)%2][(index+11)/2] = data8[3];

				data8[0] = p_tiled_addr[trans_addr + 64 * k + 12];
				data8[1] = p_tiled_addr[trans_addr + 64 * k + 13];
				data8[2] = p_tiled_addr[trans_addr + 64 * k + 14];
				data8[3] = p_tiled_addr[trans_addr + 64 * k + 15];

				pUVAddr[(index+12)%2][(index+12)/2] = data8[0];
				pUVAddr[(index+13)%2][(index+13)/2] = data8[1];
				pUVAddr[(index+14)%2][(index+14)/2] = data8[2];
				pUVAddr[(index+15)%2][(index+15)/2] = data8[3];
            }
        }
    }
}

}  // namespace android
