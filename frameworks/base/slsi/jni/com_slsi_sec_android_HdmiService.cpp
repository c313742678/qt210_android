#define LOG_TAG "HDMIStatusService"

#include "jni.h"
#include "JNIHelp.h"
#include <cutils/log.h>

#if defined(BOARD_USES_HDMI)
#include "SecHdmiService.h"
#endif

namespace android {

/*
 * Class:     com_slsi_sec_android_HdmiService
 * Method:    setHdmiCableStatus
 * Signature: (I)V
 */
static void com_slsi_sec_android_HdmiService_setHdmiCableStatus
  (JNIEnv *env, jobject obj, jint i)
{
    int result = 0;

#if defined(BOARD_USES_HDMI)
    //LOGD("%s HDMI status: %d", __func__, i);
    (SecHdmiService::getInstance())->setHdmiCableStatus(i);
#else
    return;
#endif

    //return result;
}

/*
 * Class:     com_slsi_sec_android_setHdmiMode
 * Method:    setHdmiMode
 * Signature: (I)V
 */
static void com_slsi_sec_android_HdmiService_setHdmiMode
  (JNIEnv *env, jobject obj, jint i)
{
    int result = 0;

#if defined(BOARD_USES_HDMI)
    (SecHdmiService::getInstance())->setHdmiMode(i);
#else
    return;
#endif
}

/*
 * Class:     com_slsi_sec_android_setHdmiResolution
 * Method:    setHdmiResolution
 * Signature: (I)V
 */
static void com_slsi_sec_android_HdmiService_setHdmiResolution
(JNIEnv *env, jobject obj, jint i)
{
    int result = 0;

#if defined(BOARD_USES_HDMI)
    (SecHdmiService::getInstance())->setHdmiResolution(i);
#else
    return;
#endif
}

/*
 * Class:     com_slsi_sec_android_setHdmiHdcp
 * Method:    setHdmiHdcp
 * Signature: (I)V
 */
static void com_slsi_sec_android_HdmiService_setHdmiHdcp
(JNIEnv *env, jobject obj, jint i)
{
    int result = 0;

#if defined(BOARD_USES_HDMI)
    (SecHdmiService::getInstance())->setHdmiHdcp(i);
#else
    return;
#endif
}

/*
 * Class:     com_slsi_sec_android_HdmiService
 * Method:    initHdmiService
 * Signature: ()V
 */
static void com_slsi_sec_android_HdmiService_initHdmiService
  (JNIEnv *env, jobject obj)
{
#if defined(BOARD_USES_HDMI)
    LOGI("%s ", __func__);
    (SecHdmiService::getInstance())->init();
#else
    return;
#endif
    //return result;
}

static JNINativeMethod gMethods[] = {
    {"setHdmiCableStatus", "(I)V", (void*)com_slsi_sec_android_HdmiService_setHdmiCableStatus},
    {"setHdmiMode", "(I)V", (void*)com_slsi_sec_android_HdmiService_setHdmiMode},
    {"setHdmiResolution", "(I)V", (void*)com_slsi_sec_android_HdmiService_setHdmiResolution},
    {"setHdmiHdcp", "(I)V", (void*)com_slsi_sec_android_HdmiService_setHdmiHdcp},
    {"initHdmiService", "()V", (void*)com_slsi_sec_android_HdmiService_initHdmiService},
};

int register_com_samsung_sec_android_HdmiService(JNIEnv* env)
{
     jclass clazz = env->FindClass("com/slsi/sec/android/HdmiService");

     if (clazz == NULL)
     {
         LOGE("Can't find com/slsi/sec/android/HdmiService");
         return -1;
     }

     return jniRegisterNativeMethods(env, "com/slsi/sec/android/HdmiService",
                                     gMethods, NELEM(gMethods));
}

} /* namespace android */

